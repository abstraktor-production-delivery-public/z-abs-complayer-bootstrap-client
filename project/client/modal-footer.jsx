
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


class ModalFooter extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.children, nextProps.children);
  }
  
  render() {
    return (
      <div className="modal-footer">
        {this.props.children}
      </div>
    );
  }
}


module.exports = ModalFooter;
