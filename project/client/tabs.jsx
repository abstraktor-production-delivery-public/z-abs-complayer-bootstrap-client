
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


class Tabs extends ReactComponentBase {
  constructor(props) {
    super(props, {
      isDragging: false,
      dragEventKey: ''
    });
    this.boundResize = this._resize.bind(this);
    this.ref = React.createRef();
    this.depth = 0;
  }
  
  didMount() {
    window.addEventListener('resize', this.boundResize);
    this.resizeObserver = new ResizeObserver((entries) => {
      this._resize(true);
    });
    this.resizeObserver.observe(this.ref.current);
    this._resize(true);
    this._calculateTabsDepth();
  } 
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.activeKey, nextProps.activeKey)
      || !this.shallowCompare(this.props.darkMode, nextProps.darkMode)
      || !this.shallowCompare(this.props.children, nextProps.children);
  }
  
  didUpdate() {
    this._resize(true);
  }
  
  willUnmount() {
    window.removeEventListener('resize', this.boundResize);
  }
  
  __resize() {
    if(this.ref.current) {
      const tabs = this.ref.current;
      const navigation = tabs.firstChild;
      const content = navigation.nextSibling;
      const borderWidthText = window.getComputedStyle(content).borderBottomWidth;
      const borderWidth = borderWidthText.endsWith('px') ? Number.parseFloat(borderWidthText.substring(0, borderWidthText.length - 2)) : 0;
      const contentHeight = tabs.clientHeight - navigation.clientHeight - borderWidth;
      content.setAttribute('style', `height:${contentHeight}px!important`);
      //content.setAttribute('style', `height:calc(100% - ${navigation.clientHeight}px) !important`);
      if(this.props.onResize) {
        this.props.onResize(contentHeight);
      }
    }
  }
  
  _resize(fromCode) {
    if(0 === this.depth) {
      this.__resize();
    }
    else {
      setTimeout(() => {
        this.__resize();
      }, this.depth*10);
    }
  }
  
  _calculateTabsDepth() {
    if(this.ref.current) {
      const classNameA = this.props.darkMode ? "basic_tab_link_dark" : "basic_tab_link_light";
      let parentNode = this.ref.current.parentNode;
      let i = 0;
      let depth = 0;
      while(parentNode) {
        if('function' === typeof parentNode.getAttribute) {
          const name = parentNode.getAttribute('name');
          if(classNameA === name) {
            ++depth;
          }
        }
        parentNode = parentNode.parentNode;
      }
      this.depth = depth;
    }
  }

  renderTabTitle(child, index) {
    const eventKey = undefined === child.props.eventKey ? index : child.props.eventKey;
    return (
      <a id={child.props.id} className={this.theme(this.props.darkMode, "basic_tab_link")} href={child.props.tab} draggable={this.props.draggable}
        onClick={(evt) => {
          evt.stopPropagation();
          evt.preventDefault();
          if(this.props.onSelect) {
            this.props.onSelect(eventKey);
          }
        }}
        {...(this.props.draggable && {
          onDragStart:(evt) => {
            evt.dataTransfer.setData('text', eventKey);
          },
          onDragOver:(evt) => {
            evt.preventDefault();
          },
          onDrop:(evt) => {
            evt.preventDefault();
            evt.stopPropagation();
            const fromEventKey = evt.dataTransfer.getData('text');
            if(eventKey !== fromEventKey) {
              child.props.onMove && child.props.onMove(fromEventKey, eventKey);
            }
          }
        })}
      >
        {child.props.title}
      </a>
    );
  }
  
  renderColorMark(colorMark) {
    if(colorMark) {
      const colorClass = `test_case_${colorMark}`;
      return (
        <div className={colorClass}></div>
      );
    }
  }
  
  renderTab(child, index, activeKey) {
    const cmpVal = undefined === child.props.eventKey ? index : child.props.eventKey;
    const isActive = activeKey === cmpVal;
    const active = isActive ? ' active' : '';
    return (
      <li role="presentation" key={index} className={active}>
        {this.renderTabTitle(child, index)}
        {this.renderColorMark(child.props.colorMark)}
      </li>
    );
  }
  
  renderTabs(activeKey) {
    const tabs = this.props.children.map((child, index) => {
      return this.renderTab(child, index, activeKey);
    });
    return (
      <ul className={this.theme(this.props.darkMode, ["basic_nav", "basic_nav_tabs"])}>
        {tabs}
      </ul>
    );
  }
    
  renderContent(child, index, activeKey) {
    const cmpVal = undefined === child.props.eventKey ? index : child.props.eventKey;
    const scroll = !!child.props.overflowScroll ? ' tab_pan_scroll' : '';
    const active = (activeKey === cmpVal ? ' active' : '');
    return (
      <div key={index} className={`tab_pane basic_same_size_as_parent${active}${scroll}`} >
        {child}
      </div>
    );
  }
  
  renderContents(activeKey) {
    const contents = this.props.children.map((child, index) => {
      return this.renderContent(child, index, activeKey);
    });
    const className = this.props.darkMode ? "basic_tab_content_dark" : "basic_tab_content_light";
    return (
      <div className={className}>
        {contents}
      </div>
    );
  }
  
  render() {
    const className = this.props.className ? ` ${this.props.className}` : '';
    const classNameA = this.props.darkMode ? "basic_tab_link_dark" : "basic_tab_link_light";
    return (
      <div name={classNameA} id={this.props.id} ref={this.ref} className={`basic_same_size_as_parent${className}`}>
        {this.renderTabs(this.props.activeKey)}
        {this.renderContents(this.props.activeKey)}
      </div>
    );
  }
}


module.exports = Tabs;
