
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


class ModalHeader extends ReactComponentBase {
  constructor(props) {
    super(props); 
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.closeButton, nextProps.closeButton)
      || !this.shallowCompare(this.props.children, nextProps.children);
  }
  
  renderCloseButton() {
    if(this.props.closeButton) {
      return (
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
          <span className="sr-only">Close</span>
        </button>
      );
    }
  }
  
  render() {
    return (
      <div className="modal-header">
        {this.renderCloseButton()}
        {this.props.children}
      </div>
    );
  }
}


module.exports = ModalHeader;
