
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class RealtimeZoomDisplay extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.zoom, nextProps.zoom);
  }
  
  render() {
    const zoom = Math.floor(100 * this.props.zoom);
    return (
      <div className="realtime_zoom_display">
        <p className="realtime_zoom_display" ref={this.refZoomDisplay}>
          {`${zoom}%`}
        </p>
      </div>
    );
  }
}
