
'use strict';

import Const from './realtime-zoom-const';
import RealtimeButton from '../realtime-button';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class RealtimeButtonZoomOut extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.zoom, nextProps.zoom);
  }
  
  render() {
    let zoom = this.props.zoom;
    return (
      <RealtimeButton size="btn-xs" glyphicons={['glyphicon-zoom-out']} colorMark={this.props.colorMark} styles={[null]} toolTipContent={'out'} toolTipHeading="Zoom"
        onAction={() => {
          let i = 0;
          for(; i < Const.zoom.length; ++i) {
            if(Const.zoom[i] === zoom) {
              i -= 1;
              break;
            }
            else if(Const.zoom[i] > zoom) {
              break; 
            }
          }
          if(i >= 0) {
            zoom = Const.zoom[i];
          }
          this.props.onAction(zoom);
        }}
      />
    );
  }
}
