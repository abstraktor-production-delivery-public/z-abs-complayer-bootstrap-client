
'use strict';


class RealtimeZoomConst {}

RealtimeZoomConst.zoom = [0.3, 0.5, 0.65, 0.8, 0.9, 1.0, 1.1, 1.2, 1.33, 1.5, 1.70, 2.0, 2.4, 3.0, 4.0, 5.0];


module.exports = RealtimeZoomConst;
