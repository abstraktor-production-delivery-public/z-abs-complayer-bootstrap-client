
'use strict';

import Button from '../button';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class RealtimeButton extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.buttonValue, nextProps.buttonValue)
      || !this.shallowCompare(this.props.styles, nextProps.styles)
      || !this.shallowCompare(this.props.colorMark, nextProps.colorMark)
      || !this.shallowCompare(this.props.toolTipContent, nextProps.toolTipContent);
  }
  
  _renderSpan(glyphicon, index, active) {
    const foundStyle = this.props.styles[index];
    const newStyle = foundStyle ? {style: foundStyle} : {};
    if(Array.isArray(glyphicon)) {
      glyphicon = glyphicon[(active ? 0 : 1)];
    }
    let glyph = `${glyphicon} `;
    let text = null;
    if(glyphicon.startsWith('TEXT:')) {
      glyph = '';
      text = glyphicon.substring(5);
    }
    return (
      <span key={index} className={`glyphicon ${glyph}${this.props.colorMark}`} aria-hidden="true" {...newStyle}>
        {text}
      </span>
    );
  }
  
  _renderSpans(active) {
    let index = -1;
    const spans = this.props.glyphicons.map((glyphicon) => {
      return this._renderSpan(glyphicon, ++index, active);
    });
    return (
      <>
        {spans}
      </>
    );
  }
  
  render() {
    let active = false;
    const toolTipContent = this.props.toolTipContent;
    let toolTipContents = [];
    if(Array.isArray(toolTipContent)) {
      toolTipContents = toolTipContent;
    }
    else {
      toolTipContents.push(toolTipContent);
      toolTipContents.push(toolTipContent);
    }
    const toolTipHeading = this.props.toolTipHeading;
    let toolTipHeadings = [];
    if(Array.isArray(toolTipHeading)) {
      toolTipHeadings = toolTipHeading;
    }
    else {
      toolTipHeadings.push(toolTipHeading);
      toolTipHeadings.push(toolTipHeading);
    }
    const size = this.props.size ? this.props.size : 'btn-sm';
    return (
      <Button size={size} placement="bottom" heading={toolTipHeadings[(active ? 0 : 1)]} content={toolTipContents[(active ? 0 : 1)]}
        onClick={(e) => {
          this.props.onAction(e);
        }}
      >
        {this._renderSpans(active)}
      </Button>
    );
  }
}
