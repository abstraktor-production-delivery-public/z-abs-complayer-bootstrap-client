
'use strict';

import RealtimeButtonWidthDecrease from './realtime-width/realtime-button-width-decrease';
import RealtimeButtonWidthIncrease from './realtime-width/realtime-button-width-increase';
import RealtimeWidthDisplay from './realtime-width/realtime-width-display';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class RealtimeWidth extends ReactComponentBase {
  constructor(props) {
    super(props, {
      width: 180
    });
  }
  
  didMount() {
    this.updateState({width: {$set: this.props.width}});
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.width, nextProps.width)
      || !this.shallowCompare(this.props.colorMark, nextProps.colorMark)
      || !this.shallowCompare(this.state.width, nextState.width);
  }
  
  didUpdate() {
    this.updateState({width: {$set: this.props.width}});
    this.didUpdate = undefined;
  }
  
  render() {
    return (
      <>
        <RealtimeButtonWidthDecrease width={this.state.width} colorMark={this.props.colorMark}
          onAction={(width) => {
            this.updateState({width: {$set: width}});
            this.props.onAction(width);
          }}
        />
        <RealtimeWidthDisplay width={this.state.width} colorMark={this.props.colorMark} />
        <RealtimeButtonWidthIncrease width={this.state.width} colorMark={this.props.colorMark}
          onAction={(width) => {
            this.updateState({width: {$set: width}});
            this.props.onAction(width);
          }}
        />
      </>
    );
  }
}
