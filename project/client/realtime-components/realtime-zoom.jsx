
'use strict';

import RealtimeButtonZoomIn from './realtime-zoom/realtime-button-zoom-in';
import RealtimeButtonZoomOut from './realtime-zoom/realtime-button-zoom-out';
import RealtimeZoomDisplay from './realtime-zoom/realtime-zoom-display';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class RealtimeZoom extends ReactComponentBase {
  constructor(props) {
    super(props, {
      zoom: 1.0
    });
  }
  
  didMount() {
    this.updateState({zoom: {$set: this.props.zoom}});
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.zoom, nextProps.zoom)
      || !this.shallowCompare(this.props.colorMark, nextProps.colorMark)
      || !this.shallowCompare(this.state.zoom, nextState.zoom);
  }
  
  didUpdate() {
    this.updateState({zoom: {$set: this.props.zoom}});
    this.didUpdate = undefined;
  }
  
  render() {
    return (
      <>
        <RealtimeButtonZoomOut zoom={this.state.zoom} colorMark={this.props.colorMark}
          onAction={(zoom) => {
            this.updateState({zoom: {$set: zoom}});
            this.props.onAction(zoom);
          }}
        />
        <RealtimeZoomDisplay zoom={this.state.zoom} colorMark={this.props.colorMark} />
        <RealtimeButtonZoomIn zoom={this.state.zoom} colorMark={this.props.colorMark}
          onAction={(zoom) => {
            this.updateState({zoom: {$set: zoom}});
            this.props.onAction(zoom);
          }}
        />
      </>
    );
  }
}
