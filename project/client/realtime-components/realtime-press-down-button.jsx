
'use strict';

import Button from '../button';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class RealtimePressDownButton extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.textNodes = new Map();
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.buttonValue, nextProps.buttonValue)
      || !this.shallowCompare(this.props.buttonIndex, nextProps.buttonIndex)
      || !this.shallowCompare(this.props.styles, nextProps.styles)
      || !this.shallowCompare(this.props.colorMark, nextProps.colorMark)
      || !this.shallowCompare(this.props.toolTipContent, nextProps.toolTipContent);
  }
  
  _getSpanData(index, active) {
    const foundStyle = this.props.styles[index];
    const colorMark = this.props.colorMark ? this.props.colorMark : '';
    const span = {
      glyph: '',
      text: '',
      style: foundStyle ? {style: foundStyle} : {},
      colorMark: colorMark
    };
    const glyphicons = this.props.glyphicons[index];
    const glyphicon = Array.isArray(glyphicons) ? glyphicons[(active ? 0 : 1)] : glyphicons;
    if(glyphicon.startsWith('TEXT:')) {
      span.text = glyphicon.substring(5);
    }
    else {
      span.glyph = glyphicon;
    }
    return span;
  }
  
  renderSpan(index, active) {
    const span = this._getSpanData(index, active);
    const glyph = span.glyph ? ' ' + span.glyph : '';
    const colorMark = span.colorMark ? (' ' + (Array.isArray(span.colorMark) ? span.colorMark[index] : span.colorMark)) : '';
    return (
      <span key={index} className={`glyphicon${glyph}${colorMark}`} aria-hidden="true" {...span.style}>
        {span.text}
      </span>
    );
  }
  
  renderSpans(active) {
    let index = -1;
    const spans = this.props.glyphicons.map(() => {
      return this.renderSpan(++index, active);
    });
    return (
      <>
        {spans}
      </>
    );
  }
  
  _onAction(value, e) {
    this.props.onAction(value, () => {
      const target = 'button' === e.target.type ? e.target : e.target.parentNode;
      if(target) {
        const active = target.classList.toggle('active');
        const length = this.props.glyphicons.length;
        for(let i = 0; i < length; ++i) {
          const span = this._getSpanData(i, active);
          const spanNode = target.childNodes[i];
          spanNode.classList.remove(...spanNode.classList);
          spanNode.classList.add('glyphicon');
          if(span.glyph) {
            spanNode.classList.add(span.glyph);
          }
          if(span.colorMark) {
            spanNode.classList.add(span.colorMark);
          }
          if(span.text && spanNode.firstChild && spanNode.firstChild.textContent !== span.text) {
            const newNode = this.textNodes.has(span.text) ? this.textNodes.get(span.text) : document.createTextNode(span.text);
            const oldNode = spanNode.firstChild;
            spanNode.replace(oldNode, newNode);
            if(!this.textNodes.has(oldNode.textContent)) {
              this.textNodes.set(oldNode.textContent, oldNode);
            }
          }
        }
      }
    });
  }
  
  render() {
    const toolTipContent = this.props.toolTipContent;
    let toolTipContents = [];
    if(Array.isArray(toolTipContent)) {
      toolTipContents = toolTipContent;
    }
    else {
      toolTipContents.push(toolTipContent);
      toolTipContents.push(toolTipContent);
    }
    const toolTipHeading = this.props.toolTipHeading;
    let toolTipHeadings = [];
    if(Array.isArray(toolTipHeading)) {
      toolTipHeadings = toolTipHeading;
    }
    else {
      toolTipHeadings.push(toolTipHeading);
      toolTipHeadings.push(toolTipHeading);
    }
    const buttonIndex = undefined !== this.props.buttonIndex ? Number.parseInt(this.props.buttonIndex) : undefined;
    let buttonValue = undefined !== buttonIndex ? this.props.buttonValue[buttonIndex] : this.props.buttonValue;
    let active = buttonValue;
    const onLoad = this.props.onLoad ? {onLoad: this.props.onLoad} : {};
    const size = this.props.size ? this.props.size : 'btn-sm';
    return (
      <Button size={size} active={active} placement="bottom" heading={toolTipHeadings[(active ? 0 : 1)]} content={toolTipContents[(active ? 0 : 1)]}
        onClick={(e) => {
          buttonValue = !buttonValue;
          if(undefined !== buttonIndex) {
            const values = this.deepCopy(this.props.buttonValue);
            values[buttonIndex] = buttonValue;
            this._onAction(values, e);
          }
          else {
            this._onAction(buttonValue, e);
          }
        }}
        {...onLoad}
      >
        {this.renderSpans(active)}
      </Button>
    );
  }
}
