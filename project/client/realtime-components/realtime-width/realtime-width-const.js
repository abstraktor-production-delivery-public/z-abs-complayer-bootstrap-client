
'use strict';


class RealtimeWidthConst {}

RealtimeWidthConst.WIDTH_INC = 30;
RealtimeWidthConst.WIDTH_MIN = 90;
RealtimeWidthConst.WIDTH_MAX = 750;


module.exports = RealtimeWidthConst;
