
'use strict';

import Const from './realtime-width-const';
import RealtimeButton from '../realtime-button';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class RealtimeButtonWidthDecrease extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.width, nextProps.width);
  }
  
  render() {
    let width = this.props.width;
    return (
      <RealtimeButton size="btn-xs" glyphicons={['glyphicon-resize-horizontal', 'glyphicon-minus-sign']} colorMark={this.props.colorMark} styles={[{top:2},{top:7,left:-2,width:0,transform:'scale(0.5)'}]} toolTipContent={'decrease'} toolTipHeading="Width"
        onAction={() => {
          if(Const.WIDTH_MIN < width) {
            width -= Const.WIDTH_INC;
          }
          this.props.onAction(width);
        }}
      />
    );
  }
}
