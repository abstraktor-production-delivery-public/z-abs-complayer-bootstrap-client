
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class RealtimeWidthDisplay extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.width, nextProps.width);
  }
  
  render() {
    return (
      <div className="realtime_width_display">
        <p className="realtime_width_display" ref={this.refZoomDisplay}>
          {this.props.width}
        </p>
      </div>
    );
  }
}
