
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import GuidGenerator from 'z-abs-corelayer-cs/clientServer/guid-generator';
import React from 'react';
import ReactDOM from 'react-dom';


class Modal extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.id = this.props.id ? this.props.id : GuidGenerator.create();
    this.idHash = `#${this.id}`;
    this.hideableChildren = [];
  }
  
  setHideables(hideables) {
    this.hideableChildren = hideables;
  }
  
  didMount() {
    $(this.idHash).on('hidden.bs.modal', (e) => {
      this.props.onHide && this.props.onHide(e);
      this.hideableChildren.forEach((hideableChild) => {
        hideableChild.current.onHide();
      });
    });
    $(this.idHash).on('shown.bs.modal', (e) => {
      this.props.onShown && this.props.onShown(e);
    });
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.id, nextProps.id)
      || !this.shallowCompare(this.props.show, nextProps.show)
      || !this.shallowCompare(this.props.width, nextProps.width)
      || !this.shallowCompare(this.props.className, nextProps.className)
      || !this.shallowCompare(this.props.children, nextProps.children);
  }
  
  didUpdate(prevProps, prevState) {
    if(!prevProps.show && this.props.show) {
      $(this.idHash).modal('show');
    }
    else if(prevProps.show && !this.props.show) {
      $(this.idHash).modal('hide');
    }
  }
  
  render() {
    const className = window.abstractorTestOptimization ? 'modal' : 'modal fade';
    const style = {};
    if(this.props.width) {
      style.width = this.props.width;
    }
    return ReactDOM.createPortal(
      <div className={className} id={this.id} tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div className={`modal-dialog${this.props.className ? ' ' + this.props.className : ''}`} role="document" style={style}>
          <div className="modal-content">
            {this.props.children}
          </div>
        </div>
      </div>,
      document.body
    );
  }
}


module.exports = Modal;
