
'use strict';

  
class ScrollListFrame {
  constructor(name, onScroll, onAutoScroll, onResize, onScrollLeft) {
    this.name = name;
    this.onScroll = onScroll;
    this.onAutoScroll = onAutoScroll;
    this.onResize = onResize;
    this.onScrollLeft = onScrollLeft;
    this.size = 0;
    this.height = 0;
    this.scrollbarHeight = 0;
    this.autoScroll = false;
    
    this.scrollPositionPrevious = 0.0;
    this.scrollPositionCurrent = 0.0;
    this.scrollPositionMouseDown = 0.0;
    this.scrollPositionMouseMove = 0.0;
    
    this.waitForScroll = false;
    this.mouseDown = false;
    this.delta = 0.0;
    this.deltaWheel = 0.0;
    
    this.listDivFrame = document.createElement('div');
    this.listDivFrame.classList.add('scroll_list_frame');
    this.listDivView = this.listDivFrame.appendChild(document.createElement('div'));
    this.listDivView.classList.add('scroll_list_view');
    this.listDiViewContent = this.listDivView.appendChild(document.createElement('div'));
    this.listDiViewContent.classList.add('scroll_list_content');
    
    this.listDivHide = this.listDivFrame.appendChild(document.createElement('div'));
    this.listDivHide.classList.add('scroll_list_hide');
    
    this.listDivScroller = this.listDivFrame.appendChild(document.createElement('div'));
    this.listDivScroller.classList.add('scroll_list_scroller');
    this.listDivFake = this.listDivScroller.appendChild(document.createElement('div'));
    this.listDivFake.classList.add('scroll_list_fake');

    this.boundMousedown = this._Mousedown.bind(this);
    this.boundMousemove = this._Mousemove.bind(this);
    this.boundMousemoveView = this._MousemoveView.bind(this);
    this.boundWheel = this._wheel.bind(this);
    this.listDivHide.addEventListener('mousedown', this.boundMousedown, {capture: true, passive: true});
    this.listDivView.addEventListener('wheel', this.boundWheel, {capture: true, passive: true});
    if(this.onScrollLeft) {
      this.boundScroll = this._scrollHorizontal.bind(this);
      this.listDivView.addEventListener('scroll', this.boundScroll, {capture: true, passive: true});
    }
    this.resizeObserver = new ResizeObserver((entries) => {
      if(this._onResize()) {
        this.onResize(0);
      }
    });
    this.resizeObserver.observe(this.listDivView);
  }
  
  _onResize() {
    const previousHeight = this.height;
    this.height = this.listDivView.clientHeight;
    this.scrollbarHeight = this.listDivFake.clientHeight - this.height;
    this.listDivScroller.setAttribute('style', `height:${this.height}px;`);
    return previousHeight !== this.height;
  }
  
  init(autoScroll, cb) {
    this.autoScroll = autoScroll;
    return this._onResize();
  }

  exit() {
    this.listDivView.removeEventListener('wheel', this.boundWheel, {capture: true, passive: true});
    this.listDivHide.removeEventListener('mousedown', this.boundMousedown, {capture: true, passive: true});
    if(this.onScrollLeft) {
      this.listDivHide.removeEventListener('scroll', this.boundScroll, {capture: true, passive: true});
    }
    this.resizeObserver.disconnect();
  }

  frame() {
    return this.listDivFrame;
  }
  
  list() {
    return this.listDiViewContent;
  }
  
  resize(size) {
    this.size = size;
    this.listDivFake.setAttribute('style', `height:${size}px;`);
  }
  
  lastPos() {
    return Math.max(this.size - this.height, 0);
  }
  
  currentPos() {
    return this.scrollPositionCurrent;
  }
  
  scroll(pos) {
    this.scrollPositionPrevious = this.scrollPositionCurrent;
    this.scrollPositionCurrent = pos;
    this.listDivScroller.scrollTop = this.scrollPositionCurrent;
  }
  
  _scroll(delta) {
    this.delta = delta;
    if(!this.waitForScroll) {
      this.waitForScroll = true;
      requestAnimationFrame(() => {
        const scrollDelta = this.delta / this.height * this.size;
        this.scrollPositionMouseMove = this.scrollPositionMouseDown + scrollDelta;
        if(0 >= this.scrollPositionMouseMove) {
          this.scrollPositionPrevious = this.scrollPositionCurrent;
          this.scrollPositionCurrent = 0;
        }
        else {
          const lastPosition = this.lastPos();
          if(lastPosition <= this.scrollPositionMouseMove) {
            this.scrollPositionPrevious = this.scrollPositionCurrent;
            this.scrollPositionCurrent = lastPosition;
          }
          else {
            this.scrollPositionPrevious = this.scrollPositionCurrent;
            this.scrollPositionCurrent = this.scrollPositionMouseMove;
          }
        }
        this.onScroll();
        this.listDivScroller.scrollTop = this.scrollPositionCurrent;
        if(this.autoScroll && this.scrollPositionPrevious > this.scrollPositionCurrent) {
          setTimeout(() => {
            this.onAutoScroll(false);
          });
        }
        this.waitForScroll = false;
      });
    }
  }
  
  _Mousedown(e) {
    this.mouseDown = true;
    this.mouseDownPosition = e.screenY;
    this.scrollPositionMouseDown = this.scrollPositionCurrent;
    this.scrollPositionMouseMove = this.scrollPositionCurrent;
    this.listDivHide.addEventListener('mousemove', this.boundMousemove, {capture: true, passive: true});
    document.addEventListener('mousemove', this.boundMousemoveView, {capture: true, passive: false});
  }
  
  _MousemoveView(e) {
    if(this.mouseDown) {
      e.preventDefault();
      this._Mousemove(e);
    }
  }
  
  _Mousemove(e) {
    if(1 !== e.buttons) {
      this.mouseDown = false;
      document.removeEventListener('mousemove', this.boundMousemoveView, {capture: true, passive: false});
      this.listDivHide.removeEventListener('mousemove', this.boundMousemove, {capture: true, passive: true});
      return;
    }
    if(this.mouseDown) {
      if(!e.altKey) {
        this._scroll(e.screenY - this.mouseDownPosition);
      }
      else {
        this._scroll((e.screenY - this.mouseDownPosition) / 2.0);
      }
    }
  }
  
  _wheel(e) {
    if(!e.ctrlKey) {
      if(!e.altKey) {
        this.deltaWheel += e.deltaY >= 0.0 ? 100.0 : -100.0;
      }
      else {
        this.deltaWheel += e.deltaY >= 0.0 ? 10.0 : -10.0;
      }
      if(!this.waitForScroll) {
        this.waitForScroll = true;
        requestAnimationFrame(() => {
          const newPosition = this.scrollPositionCurrent + this.deltaWheel;
          if(0 >= newPosition) {
             this.scrollPositionPrevious = this.scrollPositionCurrent;
            this.scrollPositionCurrent = 0;
          }
          else {
            const lastPosition = this.lastPos();
            if(lastPosition <= newPosition) {
              this.scrollPositionPrevious = this.scrollPositionCurrent;
              this.scrollPositionCurrent = lastPosition;
            }
            else {
              this.scrollPositionPrevious = this.scrollPositionCurrent;
              this.scrollPositionCurrent = newPosition;
            }
          }
          this.onScroll(this.scrollPositionCurrent);
          this.listDivScroller.scrollTop = this.scrollPositionCurrent;
          if(this.autoScroll && this.scrollPositionPrevious > this.scrollPositionCurrent) {
            setTimeout(() => {
              this.onAutoScroll(false);
            });
          }
          this.waitForScroll = false;
          this.deltaWheel = 0.0;
        });
      }
    }
  }
  
  _scrollHorizontal(e) {
    if(e.currentTarget) {
      this.onScrollLeft(e.currentTarget.scrollLeft);
    }
  }
}


module.exports = ScrollListFrame;
