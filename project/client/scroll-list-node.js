 
'use strict';


class ScrollListNode {
  static STATE_NONE = 0;
  static STATE_VISIBLE = 1;
  static STATE_HIDDEN = 2;
  
  constructor(template, buffer) {
    this.template = template;
    this.buffer = buffer;
    this.height = 0;
    this.startPos = 0;
    this.stopPos = 0;
    this.renderedIndex = -1;
    this.index = 0;
    this.node = null;
    this.state = ScrollListNode.STATE_NONE;
  }
  
  visible() {
    return 0 !== this.height;
  }
  
  calculateHeight() {
    this.height = this.template.calculateHeight(this.buffer);
  }
  
  create() {
    this.state = ScrollListNode.STATE_VISIBLE;
    const data = this.template.restore(this.buffer);
    this.node = this.template.create(data);
    return this.node;
  }
  
  hide() {
    this.state = ScrollListNode.STATE_HIDDEN;
    if(this.node) {
      this.template.destroy(this.node);
      this.node = null;
    }
  }
  
  remove() {
    this.renderedIndex = -1;
    this.state = ScrollListNode.STATE_NONE;
    if(this.node) {
      this.template.destroy(this.node);
      this.node = null;
    }
  }
}


module.exports = ScrollListNode;
