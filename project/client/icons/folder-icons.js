
'use strict';


class FolderIcons {
  constructor() {
    this.folderIcons = null;
  }
  
  get(iconId) {
    if(!this.folderIcons) {
      this.folderIcons = new Map();
      this.folderIcons.set('actorjs-project', {open: '/abs-images/icon/abstraktor_project_open.svg', closed: '/abs-images/icon/abstraktor_project_closed.svg'});
      this.folderIcons.set('actorjs-project-not-found', {open: '/abs-images/icon/abstraktor_project_not_found_open.svg', closed: '/abs-images/icon/abstraktor_project_not_found_closed.svg'});
      this.folderIcons.set('actorjs-static', {open: '/abs-images/icon/abstraktor_project_open.svg', closed: '/abs-images/icon/abstraktor_project_closed.svg'});
      this.folderIcons.set('actorjs-unknown', {open: '/abs-images/icon/folder_unknown_open.svg', closed: '/abs-images/icon/folder_unknown_closed.svg'});
      this.folderIcons.set('actorjs-multi', {open: '/abs-images/icon/folder_multi_open.svg', closed: '/abs-images/icon/folder_multi_closed.svg'});
      this.folderIcons.set('actorjs', {open: '/abs-images/icon/abstraktor_open.svg', closed: '/abs-images/icon/abstraktor_closed.svg'});
      this.folderIcons.set('css', {open: '/abs-images/icon/folder_css_open.svg', closed: '/abs-images/icon/folder_css_closed.svg'});
      this.folderIcons.set('html', {open: '/abs-images/icon/folder_html_open.svg', closed: '/abs-images/icon/folder_html_closed.svg'});
      this.folderIcons.set('js', {open: '/abs-images/icon/folder_js_open.svg', closed: '/abs-images/icon/folder_js_closed.svg'});
      this.folderIcons.set('json', {open: '/abs-images/icon/folder_json_open.svg', closed: '/abs-images/icon/folder_json_closed.svg'});
      this.folderIcons.set('jsx', {open: '/abs-images/icon/folder_jsx_open.svg', closed: '/abs-images/icon/folder_jsx_closed.svg'});
      this.folderIcons.set('test-case', {open: '/abs-images/icon/folder_test_case_open.svg', closed: '/abs-images/icon/folder_test_case_closed.svg'});
      this.folderIcons.set('test-suite', {open: '/abs-images/icon/folder_test_suite_open.svg', closed: '/abs-images/icon/folder_test_suite_closed.svg'});
      this.folderIcons.set('test-plan', {open: '/abs-images/icon/folder_test_plan_open.svg', closed: '/abs-images/icon/folder_test_plan_closed.svg'});
      this.folderIcons.set('test-project', {open: '/abs-images/icon/folder_test_project_open.svg', closed: '/abs-images/icon/folder_test_project_closed.svg'});
    }
    return this.folderIcons.get(iconId);
  }
}


module.exports = new FolderIcons();
