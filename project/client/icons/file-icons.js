
'use strict';


class FileIcons {
  constructor() {
    this.fileIcons = null;
  }
  
  get(iconId) {
    if(!this.FileIcons) {
      this.fileIcons = new Map();
      this.fileIcons.set('bld', {valid: '/abs-images/icon/build.svg', invalid: '/abs-images/icon/build_invalid.svg'});
      this.fileIcons.set('css', {valid: '/abs-images/icon/css.svg', invalid: '/abs-images/icon/css_invalid.svg'});
      this.fileIcons.set('js', {valid: '/abs-images/icon/js.svg', invalid: '/abs-images/icon/js_invalid.svg'});
      this.fileIcons.set('json', {valid: '/abs-images/icon/json.svg', invalid: '/abs-images/icon/json_invalid.svg'});
      this.fileIcons.set('jsx', {valid: '/abs-images/icon/jsx.svg', invalid: '/abs-images/icon/jsx_invalid.svg'});
      this.fileIcons.set('html', {valid: '/abs-images/icon/html.svg', invalid: '/abs-images/icon/html_invalid.svg'});
      this.fileIcons.set('md', {valid: '/abs-images/icon/md.svg', invalid: '/abs-images/icon/md_invalid.svg'});
      this.fileIcons.set('txt', {valid: '/abs-images/icon/txt.svg', invalid: '/abs-images/icon/txt_invalid.svg'});
      this.fileIcons.set('prj', {valid: '/abs-images/icon/project.svg', invalid: '/abs-images/icon/project_invalid.svg'});
      this.fileIcons.set('ukn', {valid: '/abs-images/icon/unknown.svg', invalid: '/abs-images/icon/unknown_invalid.svg'});
    }
    return this.fileIcons.get(iconId);
  }
}


module.exports = new FileIcons();
