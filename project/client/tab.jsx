
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import GuidGenerator from 'z-abs-corelayer-cs/clientServer/guid-generator';
import React from 'react';


class Tab extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.id = props.id ? props.id : GuidGenerator.create();
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.children, nextProps.children)
      || !this.shallowCompare(this.props.overflowScroll, nextProps.overflowScroll);
  }
  
  render() {
    return (
      <React.Fragment key={this.id}>
        {this.props.children}
      </React.Fragment>
    );
  }
}


module.exports = Tab;
