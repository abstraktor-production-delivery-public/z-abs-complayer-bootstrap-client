
'use strict';

import ScrollListCache from './scroll-list/scroll-list-cache';
import ScrollListFrame from './scroll-list-frame';
import ScrollListNode from './scroll-list-node';
import ScrollListZeroHeight from './scroll-list-zero-height';


class ScrollList {
  constructor(name, cbAutoScroll, funcRenderRealtime, cbScrollLeft) {
    this.name = name;
    this.cbAutoScroll = cbAutoScroll;
    this.funcRenderRealtime = funcRenderRealtime;
    this.autoScroll = true;
    this.zoom = 1.0;
    this.listDiv = null;
    this.scrollListFrame = new ScrollListFrame(name, () => {
      this.funcRenderRealtime([false, true]);
    }, (autoScroll) => {
      if(autoScroll !== this.autoScroll) {
        this.autoScroll = autoScroll;
        this.scrollListFrame.autoScroll = autoScroll;
        cbAutoScroll(autoScroll);
      }
    }, (index) => {
      this.recalculate();
      this.funcRenderRealtime([false, true]);
    }, cbScrollLeft);
    this.allNodes = [new ScrollListNode(ScrollListZeroHeight, null)];
    this.isActive = false;
    this.currentFirstIndex = 0;
    this.currentLastIndex = 0;
    this.sizeTransform = 0.0;
    this.renderedNodes = [];
  }
  
  print() {
    this.allNodes.forEach((n) => {
      n.print();
    });
  }
  
  init(scrollDiv, autoScroll, zoom) {
    scrollDiv.appendChild(this.scrollListFrame.frame());
    this.listDiv = this.scrollListFrame.list();
    this.autoScroll = autoScroll;
    this.zoom = zoom;
    this.scrollListFrame.init(autoScroll);
    this._clear();
  }
  
  update(autoScroll, zoom, recalculate) {
    const isZoom = this.zoom !== zoom;
    let doUpdate = this.autoScroll !== autoScroll || isZoom || recalculate;
    this.autoScroll = autoScroll;
    this.zoom = zoom;
    this.scrollListFrame.autoScroll = autoScroll;
    if(doUpdate) {
      this.recalculate();
      this.funcRenderRealtime([false, true]);
    }
  }
  
  active(isActive) {
    this.isActive = isActive;
  }
  
  exit() {
    this.scrollListFrame.exit();
  }
  
  _clear() {
    const allNodes = this.allNodes;
    const allNodesLength = this.allNodes.length;
    const renderedNodes = this.renderedNodes;
    const renderedNodesLength = this.renderedNodes.length;
    for(let i = 0; i < renderedNodesLength; ++i) {
      const renderedNode = renderedNodes[i];
      allNodes[renderedNode.nodeIndex].remove();
      if(renderedNode.element) {
        this.listDiv.removeChild(renderedNode.element);
        renderedNode.element = null;
      }
    }
    this.renderedNodes = [];
    this.allNodes = [new ScrollListNode(ScrollListZeroHeight, null)];
    this.funcRenderRealtime(null, () => {
      this.scrollListFrame.resize(0);
      this.scrollListFrame.scroll(0);
    });
    ScrollListCache.clear();
  }
  
  add(node) {
    if(node) {
      const allNodes = this.allNodes;
      const previous = allNodes[allNodes.length - 1];
      node.calculateHeight();
      node.index = allNodes.length;
      node.startPos = previous.stopPos;
      node.stopPos = node.startPos + node.height;
      allNodes.push(node);
    }
  }
  
  _recalculate(prev, curr) {
    curr.calculateHeight();
    curr.startPos = prev.p.stopPos;
    curr.stopPos = curr.startPos + curr.height;
    prev.p = curr;
  }
  
  recalculate() {
    const allNodes = this.allNodes;
    const nodesLastIndex = allNodes.length - 1;
    const previous = {p: allNodes[0]};
    allNodes.forEach((e, i, b) => {
      this._recalculate(previous, b[i]);
    });
  }
  
  refresh() {
    this.funcRenderRealtime([false, true]);
  }
  
  _findIndexFirst(pos) {
    pos /= this.zoom;
    const allNodes = this.allNodes;
    const allNodesLength = allNodes.length - 1;
    let first = 0;
    let last = allNodes.length - 1;
    let mid = -1;
    while(first <= last) {
      mid = (first + last) >>> 1;
      if((allNodes[mid].startPos <= pos) && (allNodes[mid].stopPos >= pos)) {
        while(mid >= 1 && allNodes[mid - 1].startPos === allNodes[mid].startPos) {
          --mid;
        }
        return mid;
      }
      else {
        if(allNodes[mid].stopPos > pos) {
          last = mid - 1;
        }
        else {
          first = mid + 1;
        }
      }
    }
  }
  
  _findIndexLast(pos) {
    pos += this.scrollListFrame.height;
    pos /= this.zoom;
    const allNodes = this.allNodes;
    const allNodesLength = allNodes.length - 1;
    let first = 0;
    let last = allNodes.length - 1;
    let mid = -1;
    while(first <= last) {
      mid = (first + last) >>> 1;
      if((allNodes[mid].startPos <= pos) && (allNodes[mid].stopPos >= pos)) {
        return mid;
      }
      else {
        if(allNodes[mid].startPos < pos) {
          first = mid + 1;
        }
        else {
          last = mid - 1;
        }
      }
    }
    return allNodesLength;
  }
  
  _getNextRenderedElement(renderedNodes, searchIndex) {
    while(searchIndex < renderedNodes.length && !renderedNodes[searchIndex].element) {
      ++searchIndex;
    }
    if(searchIndex <= renderedNodes.length) {
      return renderedNodes[searchIndex].element;
    }
    return null;
  }
  
  _setPosition(pos, rerenderAll) {
    const allNodes = this.allNodes;
    const firstIndex = this._findIndexFirst(pos);
    const lastIndex = this._findIndexLast(pos);
    if(!rerenderAll && this.currentFirstIndex === firstIndex && this.currentLastIndex === lastIndex) {
      return;
    }
    this.currentFirstIndex = firstIndex;
    this.currentLastIndex = lastIndex;
    
    // REMOVE
    const renderedNodes = this.renderedNodes;
    const renderedNodesLength = this.renderedNodes.length;
    this.renderedNodes = [];
    const newRenderedNodes = this.renderedNodes;
    if(!rerenderAll) {
      for(let i = 0; i < renderedNodesLength; ++i) {
        const renderedNode = renderedNodes[i];
        const node = allNodes[renderedNode.nodeIndex];
        if(firstIndex > renderedNode.nodeIndex || lastIndex < renderedNode.nodeIndex) {
          node.renderedIndex = -1;
          node.hide();
          if(renderedNode.element) {
            this.listDiv.removeChild(renderedNode.element);
            renderedNode.element = null;
          }
        }
        else {
          node.renderedIndex = newRenderedNodes.length;
          newRenderedNodes.push(renderedNode);
        }
      }
    }
    else {
      for(let i = 0; i < renderedNodesLength; ++i) {
        const renderedNode = renderedNodes[i];
        const node = allNodes[renderedNode.nodeIndex];
        node.renderedIndex = -1;
        node.hide();
        if(renderedNode.element) {
          this.listDiv.removeChild(renderedNode.element);
          renderedNode.element = null;
        }
      }      
    }
    
    // ADD
    if(0 !== newRenderedNodes.length) {
      const firstRenderedNodeIndex = newRenderedNodes[0].nodeIndex;
      const lastRenderedNodeIndex = newRenderedNodes[newRenderedNodes.length - 1].nodeIndex;
      let renderedIndex = 0;
      for(let i = firstIndex; i <= lastIndex; ++i, ++renderedIndex) {
        const node = allNodes[i];
        node.renderedIndex = renderedIndex;
        if(i < firstRenderedNodeIndex) { // BEFORE
          let element = null;
          if(node.visible()) {
            element = node.create();
            const firstRenderedChild = newRenderedNodes[allNodes[firstRenderedNodeIndex].renderedIndex].element;
            if(firstRenderedChild) {
              this.listDiv.insertBefore(element, firstRenderedChild);
            }
            else {
              const child = this._getNextRenderedElement(newRenderedNodes, firstRenderedNodeIndex);
              this.listDiv.insertBefore(element, child);
            }
          }
          else {
            node.hide();
          }
          ++allNodes[firstRenderedNodeIndex].renderedIndex;
          newRenderedNodes.splice(node.renderedIndex, 0, {
            nodeIndex: node.index,
            element: element
          });
        }
        else if(i > lastRenderedNodeIndex) { // AFTER
          let element = null;
          if(node.visible()) {
            element = node.create();
            this.listDiv.appendChild(element);
          }
          else {
            node.hide();
          }
          newRenderedNodes.push({
            nodeIndex: node.index,
            element: element
          });
        }
        else { // MIDDLE
          if(ScrollListNode.STATE_HIDDEN === node.state) {
            if(node.visible()) {
              const renderedNode = newRenderedNodes[node.renderedIndex];
              const element = node.create();
              renderedNode.element = element;
              const child = this._getNextRenderedElement(newRenderedNodes, node.renderedIndex + 1);
              this.listDiv.insertBefore(element, child);
            }
          }
          else {
            if(!node.visible()) {
              const renderedNode = newRenderedNodes[node.renderedIndex];
              node.hide();
              if(renderedNode.element) {
                this.listDiv.removeChild(renderedNode.element);
                renderedNode.element = null;
              }
            }
          }
        }
      }
    }
    else {
      // ADD ALL
      for(let i = firstIndex; i <= lastIndex; ++i) {
        const node = allNodes[i];
        let element = null;
        if(node.visible()) {
          element = node.create();
          this.listDiv.appendChild(element);
        }
        else {
          node.hide();
        }
        node.renderedIndex = newRenderedNodes.length;
        newRenderedNodes.push({
          nodeIndex: node.index,
          element: element
        });
      }
    }
    try {
      const sizeTransform = allNodes[firstIndex].startPos - pos/this.zoom;
      let stopPos = allNodes[firstIndex].stopPos;
      let i = 0;
      while(allNodes[firstIndex].startPos === stopPos && i < allNodes.length - 1) {
        stopPos = allNodes[firstIndex + ++i].stopPos;
      }
      this.sizeTransform = sizeTransform / (stopPos - allNodes[firstIndex].startPos);
      this.listDiv.setAttribute('style', `transform:translateY(${sizeTransform*this.zoom}px) scale(${this.zoom});`);
    } catch(a) {
      ddb.error('firstIndex:', firstIndex);
    }
  }
  
  renderRealtimeFrame(timestamp, clear, force) {
    if(!clear) {
      if(this.isActive) {
        this.scrollListFrame.resize(this.allNodes[this.allNodes.length - 1].stopPos * this.zoom);
        if(this.autoScroll) {
          const pos = this.scrollListFrame.lastPos();
          this._setPosition(pos, force);
          this.scrollListFrame.scroll(pos);
        }
        else {
          const pos = this.scrollListFrame.currentPos();
          this._setPosition(pos, force);
        }
      }
    }
    else {
      this._clear();
    }
  }
}


module.exports = ScrollList;
