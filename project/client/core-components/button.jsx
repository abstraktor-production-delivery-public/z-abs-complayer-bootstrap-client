
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import GuidGenerator from 'z-abs-corelayer-cs/clientServer/guid-generator';
import React from 'react';


class Button extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.id = this.props.id ? this.props.id : GuidGenerator.create();
    this.$popover = null;
    this.clearId = 0;
    this.refButton = props.onLoad ? React.createRef() : null;
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.active, nextProps.active)
      || !this.shallowCompare(this.props.content, nextProps.content)
      || !this.shallowCompare(this.props.disabled, nextProps.disabled)
      || !this.shallowCompare(this.props.heading, nextProps.heading)
      || !this.shallowCompare(this.props.placement, nextProps.placement)
      || !this.shallowCompare(this.props.shortcut, nextProps.shortcut);
  }
  
  didMount() {
    this._createPopover();
    if(this.refButton) {
      this.props.onLoad(this.refButton.current);
    }
  }
  
  didUpdate() {
    this._createPopover();
    if(null !== this.$popover) {
      const popover = this.$popover.data('bs.popover');
      if(null !== popover.tip()[0].lastChild.firstChild) {
        popover.tip()[0].lastChild.firstChild.lastChild.innerHTML = this._getContent();
      }
    }
  }
  
  willUnmount() {
    this.onHide();
  }
  
  onHide() {
    if(0 !== this.clearId) {
      clearTimeout(this.clearId);
      this.clearId = 0;
    }
    if(null !== this.$popover) {
      this.$popover.popover('destroy');
      this.$popover = null;
    }
  }
  
  render() {
    const placement = this.props.placement ? this.props.placement : 'bottom';
    const title = this.props.title ? { title: this.props.title } : {};
    const disabled = this.props.disabled ? ' disabled' : '';
    const active = this.props.active ? ' active' : '';
    const type = this.props.type ? ` btn-${this.props.type}` : ' btn-default';
    const className = this.props.className ? ` ${this.props.className}` : '';
    const al = this.props['aria-label'];
    const ariaLabel = al ? { "aria-label": al } : {};
    const ref = this.refButton ? {ref: this.refButton} : {};
    return (
      <button {...ref} id={this.id} type="button" className={`btn btn-xs${type}${disabled}${active}${className}`} data-toggle="popover" {...title} data-placement={placement} data-img="/abs-images/svg/AbstraktorA.svg" style={this.props.style} {...ariaLabel}
        onClick={(e) => {
          if(!this.props.disabled) {
            this.props.onClick && this.props.onClick(e);
          }
        }}>
        {this.props.children}
      </button>
    );
  }
  
  _createPopover() {
    if(!this.props.disabled && !this.$popover) {
      const self = this;
      const showDelay = this.props.showDelay ? this.props.showDelay : 1000;
      const hideDelay = this.props.hideDelay ? this.props.hideDelay : 100;
      const autoHideDelay = this.props.autoHideDelay ? this.props.autoHideDelay : 5000;
      this.$popover = $(`#${this.id}`).popover({html:true,container:'body',delay:{show:showDelay,hide:hideDelay},trigger:'hover',content:function() {
        const image = $(this).data('img');
        return `<div class="bootstrap_popover"><img class="bootstrap_popover" src="${image}" />${self._getContent()}</div>`;
      }});
      this.$popover.on('shown.bs.popover', function () {
        self.clearId = setTimeout(() => {
          if(self.$popover) {
            self.$popover.popover('hide');
          }
        }, autoHideDelay);
      });
      this.$popover.on('hidden.bs.popover', function () {
        if(0 !== self.clearId) {
          clearTimeout(self.clearId);
          self.clearId = 0;
        }
      });
    }
    if(this.props.disabled && this.$popover) {
      if(self.$popover) {
        this.$popover.popover('destroy');
        this.$popover = null;
      }
    }
  }
  
  _getContent() {
    let content = '';
    if(typeof this.props.content === 'string') {
      content = this.props.content;
    }
    else if(typeof this.props.content === 'function') {
      content = this.props.content();
    }
    let innerContent = '';
    if(typeof this.props.innerContent === 'string') {
      innerContent = this.props.innerContent;
    }
    else if(typeof this.props.innerContent === 'function') {
      innerContent = this.props.innerContent();
    }
    const shortcut = this.props.shortcut ? `<span class="bootstrap_popover"> (${this.props.shortcut})</span>` : '';
    if(this.props.heading) {
      return `<p class="bootstrap_popover"><strong>${this.props.heading}: </strong>${content + shortcut + '</p>'}${innerContent}`;  
    }
    else {
      return `<p class="bootstrap_popover">${content + shortcut + '</p>'}${innerContent}`;  
    }
  }
}


module.exports = Button;
