
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import GuidGenerator from 'z-abs-corelayer-cs/clientServer/guid-generator';
import React from 'react';


class Popover extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.id = this.props.id ? this.props.id : GuidGenerator.create();
    this.$popover = null;
    this.clearId = 0;
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.content, nextProps.content)
      || !this.shallowCompare(this.props.heading, nextProps.heading)
      || !this.shallowCompare(this.props.placement, nextProps.placement)
      || !this.shallowCompare(this.props.children, nextProps.children);
  }
  
  didMount() {
    this._createPopover();
  }
  
  didUpdate() {
    this._createPopover();
    if(null !== this.$popover) {
      const popover = this.$popover.data('bs.popover');
      if(null !== popover.tip()[0].lastChild.firstChild) {
        popover.tip()[0].lastChild.firstChild.lastChild.outerHTML = this._getContent();
      }
    }
  }
  
  willUnmount() {
    this.onHide();
  }
  
  onHide() {
    if(0 !== this.clearId) {
      clearTimeout(this.clearId);
      this.clearId = 0;
    }
    if(null !== this.$popover) {
      this.$popover.popover('destroy');
      this.$popover = null;
    }
  }
  
  render() {
    const placement = this.props.placement ? this.props.placement : 'bottom';
    const title = this.props.title ? this.props.title : '';
    const disabled = this.props.disabled ? ' disabled' : '';
    const active = this.props.active ? ' active' : '';
    const className = this.props.className ? ` ${this.props.className}` : '';
    return (
      <div id={this.id} className={`${className}${disabled}${active}`} data-toggle="popover" title={title} data-placement={placement} data-img="/abs-images/svg/AbstraktorA.svg" style={this.props.style}>
        {this.props.children}
      </div>
    );
  }
  
  _createPopover() {
    if(!this.props.disabled && !this.$popover) {
      const self = this;
      const showDelay = this.props.showDelay ? this.props.showDelay : 1000;
      const hideDelay = this.props.hideDelay ? this.props.hideDelay : 50;
      const autoHideDelay = this.props.autoHideDelay ? this.props.autoHideDelay : 5000;
      this.$popover = $(`#${this.id}`).popover({html:true,container:'body',trigger:'hover',delay:{show:showDelay,hide:hideDelay},content:function() {
        const image = $(this).data('img');
		    return `<div class="bootstrap_popover"><img class="bootstrap_popover" src="${image}" />${self._getContent()}</div>`;
      }});
      this.$popover.on('shown.bs.popover', function () {
        self.clearId = setTimeout(() => {
          if(self.$popover) {
            self.$popover.popover('hide');
          }
        }, autoHideDelay);
      });
      this.$popover.on('hidden.bs.popover', function () {
        if(0 !== self.clearId) {
          clearTimeout(self.clearId);
          self.clearId = 0;
        }
      });  
    }
    if(this.props.disabled && this.$popover) {
      if(self.$popover) {
        this.$popover.popover('destroy');
        this.$popover = null;
      }
    }
  }
  
  _getContent() {
    let content = '';
    if(typeof this.props.content === 'string') {
      content = this.props.content;
    }
    else if(typeof this.props.content === 'function') {
      content = this.props.content();
    }
    let innerContent = '';
    if(typeof this.props.innerContent === 'string') {
      innerContent = this.props.innerContent;
    }
    else if(typeof this.props.innerContent === 'function') {
      innerContent = this.props.innerContent();
    }
    const icon = this.props.icon ? this.props.icon : '';
    const shortcut = this.props.shortcut ? `<span class="bootstrap_popover"> (${this.props.shortcut})</span>` : '';
    const colon = content || shortcut ? ':' : '';
    if(!content) {
      content = ' ';
    }
    if(this.props.heading) {
      return `<p class="bootstrap_popover_heading"><strong>${this.props.heading}${colon}</p><p class="bootstrap_popover_content"></strong>${content + shortcut + '</p>'}${innerContent}`;  
    }
    else {
      return `<p class="bootstrap_popover">${content + shortcut + '</p>'}${innerContent}`;  
    }
  }
}


module.exports = Popover;
