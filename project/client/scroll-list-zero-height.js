
'use strict';


class ScrollListZeroHeight {
  constructor() {}
  
  calculateHeight(buffer) {
    return 0;
  }
  
  destroy(node) {}
}


module.exports = new ScrollListZeroHeight();
