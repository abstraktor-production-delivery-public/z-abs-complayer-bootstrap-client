
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


class ComponentMarkedTextarea extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.refTextArea = React.createRef();
    this.refDiv = React.createRef();
    this.scrollEventGuard = false;
  }
  
  scroll(scrollData) {
    const current = this.refTextArea.current;
    this.scrollEventGuard = true;
    if(0 === scrollData.line + scrollData.delta || 0 === current.rows) {
      current.scrollTop = 0;
    }
    else {
      current.scrollTop = current.scrollHeight / current.rows * (scrollData.line - 1 + scrollData.delta);
    }
  }
  
  getRows() {
    if(undefined !== this.props.value) {
      const lines = this.props.value.split('\n');
      return lines.length;
    }
    else {
       return 2;
    }
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompareArrayValues(this.props.results, nextProps.results)
      || !this.shallowCompare(this.props.value, nextProps.value)
      || !this.shallowCompare(this.props.id, nextProps.id)
      || !this.shallowCompare(this.props.className, nextProps.className);
  }
 
  renderMarkupResult() {
    if(undefined !== this.props.results) {
      const spanStyleError = {
        margin: '0px',
        padding: '0px',
        fontFamily: 'Menlo, Monaco, Consolas, \"Courier New\", monospace',
        backgroundColor: '#FFBBBB',
        fontSize: '100%',
        display: 'block',
        overflow:'hidden',
        width: '100%'
      };
      const spanStyleSuccess = {
        margin: '0px',
        padding: '0px',
        fontFamily: 'Menlo, Monaco, Consolas, \"Courier New\", monospace',
        backgroundColor: 'LavenderBlush',
        fontSize: '100%',
        display: 'block',
        overflow:'hidden',
        width: '100%'
      };
      const divStyle = {
        position: 'relative'
      };
      let index = 0;
      const lines = this.props.results.map((result) => {
        if(result.success) {
          return (
            <span key={++index} style={spanStyleSuccess}>&nbsp;</span>
          );
        }
        else {
          return (
            <span key={++index} style={spanStyleError}>&nbsp;</span>
          );
        }
      });
      return (
        <div ref={this.refDiv} style={divStyle}>
          {lines}
        </div>
      );
    }
  }
  
  renderTextarea() {
    const absoluteStyleInner = {
      position: 'absolute',
      top: '0px',
      right: '0px',
      bottom: '0px',
      left: '0px',
    };
    const textAreaStyle = {
      overflow: 'auto',
      margin: '0px',
      padding: '4px',
      fontFamily: 'Menlo, Monaco, Consolas, \"Courier New\", monospace',
      fontSize: '100%',
      background: 'transparent',
      width: '100%',
      height: '100%'
    };
    return (
      <div style={absoluteStyleInner}>
        <textarea id={this.props.id} ref={this.refTextArea} className={this.props.className} style={textAreaStyle} value={this.props.value} rows={this.getRows()} wrap="off"
          onScroll={(e) => {
            if(!this.scrollEventGuard) {
              this.props.onScroll && this.props.onScroll(e);
            }
            else {
              this.scrollEventGuard = false;
            }
            if(null !== this.refDiv.current) {
              this.refDiv.current.style.top = `-${this.refTextArea.current.scrollTop}px`;
            }
          }}
          onChange={(e) => {
            this.props.onChange && this.props.onChange(e.target.value);
          }}
        />
      </div>
    );
  }
  
  render() {
    const textareaError = {
      border: '0px',
      margin: '1px',
      padding: '4px 0px',
      overflow: 'hidden'
    };
    const absoluteStyleOuter = {
      position: 'absolute',
      top: '26px',
      right: '0px',
      bottom: '0px',
      left: '0px',
    };
    return (
      <div style={absoluteStyleOuter}>
        <div className="bootstrap_same_size_as_parent" style={textareaError}>
          {this.renderMarkupResult()}
        </div>
        {this.renderTextarea()}
      </div>
    );
  }
}

module.exports = ComponentMarkedTextarea;
