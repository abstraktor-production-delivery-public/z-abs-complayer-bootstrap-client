
'use strict';


class ScrollListCache {
  static handleText(text) {
    let id = ScrollListCache.textKey.get(text);
    if(!id) {
      id = ++ScrollListCache.id;
      ScrollListCache.textKey.set(text, id);
      ScrollListCache.numberKey.set(id, text);
    }
    return id;
  }
  
  static getText(id) {
    return ScrollListCache.numberKey.get(id);
  }
  
  static clear() {
    ScrollListCache.textKey.clear();
    ScrollListCache.numberKey.clear();
    ScrollListCache.id = 0;
  }
}


ScrollListCache.textKey = new Map();
ScrollListCache.numberKey = new Map();
ScrollListCache.id = 0;

module.exports = ScrollListCache;
