
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import GuidGenerator from 'z-abs-corelayer-cs/clientServer/guid-generator';
import React from 'react';


class Group extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.guid = GuidGenerator.create();
    this.$popover = null;
    this.clearId = 0;
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.placement, nextProps.placement)
      || !this.shallowCompare(this.props.title, nextProps.title)
      || !this.shallowCompare(this.props.disabled, nextProps.disabled)
      || !this.shallowCompare(this.props.active, nextProps.active)
      || !this.shallowCompare(this.props.className, nextProps.className)
      || !this.shallowCompare(this.props.text, nextProps.text)
      || !this.shallowCompare(this.props.children, nextProps.children);
  }
  
  didMount() {
    const self = this;
    this.$popover = $(`#${this.guid}`).popover({
      html: true,
      container: 'body',
      trigger: 'hover',
      delay: {
        show: 1000,
        hide: 0
      },
      content: function() {
        return `<div class="bootstrap_popover"><div class="bootstrap_popover">${self._getContent()}</div></div>`;
      },
      template: '<div class="popover" role="tooltip"><div class="arrow"></div><img class="bootstrap_group_pop_image" src="/abs-images/svg/AbstraktorA.svg" alt="Abstraktor Icon"><h3 class="popover-title bootstrap_group_pop_image"></h3><div class="popover-content"></div></div>'
    });
    this.$popover.on('shown.bs.popover', function () {
      const showTime = self.props.showTime ? self.props.showTime : 3000;
      if(-1 !== showTime) {
        self.clearId = setTimeout(() => {
          self.$popover.popover('hide');
        }, showTime);
      }
    });
    this.$popover.on('hidden.bs.popover', function () {
      if(0 !== self.clearId) {
        clearTimeout(self.clearId);
        self.clearId = 0;
      }
    });
  }
  
  didUpdate() {
    const popover = this.$popover.data('bs.popover');
    if(null !== popover.tip()[0].lastChild.firstChild) {
      popover.tip()[0].lastChild.firstChild.lastChild.innerHTML = this._getContent();
    }
  }
  
  willUnmount() {
    this.$popover.popover('destroy');
  }
  
  render() {
    const placement = this.props.placement ? this.props.placement : 'bottom';
    const title = this.props.title ? this.props.title : '';
    const disabled = this.props.disabled ? ' disabled' : '';
    const active = this.props.active ? ' active' : '';
    const className = this.props.className ? ` ${this.props.className}` : '';
    return (
      <g id={this.guid} className={`${className}${disabled}${active}`} title={title} data-toggle="popover" data-placement={placement}>
        {this.props.children}
      </g>
    );
  }
  
  _getContent() {
    const texts = this.props.text ? this.props.text : '';
    return texts.map((text) => {
      if(undefined === text[2]) {
        return `<strong>${text[0]}</strong> ${text[1]}<br/>`;
      }
      else {
        return `<strong class="${text[2]}">${text[0]}</strong> ${text[1]}<br/>`;
      }
    }).join('');
  }
}


module.exports = Group;
